package com.example.clientservice.exception.custom;

public class ClientExistenceException extends BadRequestException {
    public ClientExistenceException() {
        super("There is already client with such email.");
    }
}
