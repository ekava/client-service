package com.example.clientservice.exception.custom;

import org.springframework.http.HttpStatus;

public class ClientNotFoundException extends BadRequestException {
    public ClientNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Client with such id does not exist");
    }
}
