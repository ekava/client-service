package com.example.clientservice.exception;

import java.util.List;

public record ValidationErrorResponse(String errorCode,
                                      List<ErrorExtension> errors) {
    public ValidationErrorResponse(List<ErrorExtension> errorExtensions) {
        this("validation_failed", errorExtensions);
    }
}
