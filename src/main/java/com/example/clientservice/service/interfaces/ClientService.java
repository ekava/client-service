package com.example.clientservice.service.interfaces;

import com.example.clientservice.dto.ClientRequestDto;
import com.example.clientservice.dto.ClientResponseDto;
import java.util.UUID;

public interface ClientService {

    /**
     * TODO write doc
     * @param clientDto
     */
    void createClient(ClientRequestDto clientDto);

    /**
     * TODO write doc
     * @param id
     * @return
     */
    ClientResponseDto getClientById(UUID id);
}
