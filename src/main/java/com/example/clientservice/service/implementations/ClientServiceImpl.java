package com.example.clientservice.service.implementations;

import static com.example.clientservice.exception.custom.KeycloakException.UNABLE_TO_FIND_USER_IN_KEYCLOAK;
import static com.example.clientservice.exception.custom.KeycloakException.UNABLE_TO_GET_ADMIN_TOKEN;
import static com.example.clientservice.exception.custom.KeycloakException.UNABLE_TO_REGISTER_USER_IN_KEYCLOAK;

import com.example.clientservice.config.properties.KeycloakProperties;
import com.example.clientservice.dto.ClientRequestDto;
import com.example.clientservice.dto.ClientResponseDto;
import com.example.clientservice.dto.keycloak.KeycloakAssignRoleDto;
import com.example.clientservice.dto.keycloak.KeycloakClientRegisterDto;
import com.example.clientservice.exception.custom.ClientExistenceException;
import com.example.clientservice.exception.custom.ClientNotFoundException;
import com.example.clientservice.feignclient.KeycloakAdminClient;
import com.example.clientservice.feignclient.KeycloakClient;
import com.example.clientservice.mapper.ClientMapper;
import com.example.clientservice.repository.ClientRepository;
import com.example.clientservice.service.interfaces.ClientService;
import feign.FeignException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;
    private final KeycloakAdminClient keycloakAdminClient;
    private final KeycloakClient keycloakClient;
    private final KeycloakProperties keycloakProperties;

    private String getAdminToken() {
        String authHeader = "Basic " + Base64.getEncoder().encodeToString(
                String.format("%s:%s", keycloakProperties.getClientId(), keycloakProperties.getClientSecret())
                .getBytes());
        Map<String, String> formData = new HashMap<>();
        formData.put("grant_type", keycloakProperties.getGrantType());
        try {
            var tokenResponseDto = keycloakAdminClient.getToken(authHeader, formData);
            return "Bearer " + tokenResponseDto.access_token();
        } catch (FeignException ex) {
            throw UNABLE_TO_GET_ADMIN_TOKEN;
        }
    }

    private KeycloakAssignRoleDto getUserRoleDto() {
        return new KeycloakAssignRoleDto(
                keycloakProperties.getUserRoleId(),
                "user",
                keycloakProperties.getContainerId()
        );
    }

    private void registerUserInKeycloak(ClientRequestDto client) {
        var registerDto = new KeycloakClientRegisterDto(client.email(), client.password());
        String bearerToken = getAdminToken();
        try {
            keycloakClient.registerUser(bearerToken, registerDto);
            var clientId = keycloakClient.getUsersByEmail(bearerToken, client.email()).stream()
                            .findAny()
                            .orElseThrow(() -> UNABLE_TO_FIND_USER_IN_KEYCLOAK)
                            .id();
            keycloakClient.assignRole(
                    bearerToken,
                    clientId,
                    new KeycloakAssignRoleDto[] {getUserRoleDto()}
            );
        } catch (FeignException ex) {
            throw UNABLE_TO_REGISTER_USER_IN_KEYCLOAK;
        }
    }

    private void registerUserInDatabase(ClientRequestDto clientRequestDto) {
        var client = clientMapper.clientRequestDtoToClient(clientRequestDto);
        var userDto = keycloakClient.getUsersByEmail(getAdminToken(), clientRequestDto.email()).stream()
                .findAny()
                .orElseThrow(() -> UNABLE_TO_FIND_USER_IN_KEYCLOAK);
        client.setId(userDto.id());
        try {
            clientRepository.save(client);
        } catch (DataAccessException e) {
            throw new ClientExistenceException();
        }
    }


    @Override
    public void createClient(ClientRequestDto clientDto) {
        registerUserInKeycloak(clientDto);
        registerUserInDatabase(clientDto);
    }

    @Override
    public ClientResponseDto getClientById(UUID id) {
        var client = clientRepository.findById(id)
                .orElseThrow(ClientNotFoundException::new);
        return clientMapper.clientToClientResponseDto(client);
    }
}
