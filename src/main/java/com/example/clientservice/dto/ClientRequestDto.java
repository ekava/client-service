package com.example.clientservice.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * DTO for {@link com.example.clientservice.entity.Client}
 */
public record ClientRequestDto(@NotNull String name,
                               @NotNull String surname,
                               @Email String email,
                               @NotNull @Size(min = 8, max = 26) String password) {
}