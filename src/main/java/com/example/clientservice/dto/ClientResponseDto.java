package com.example.clientservice.dto;

import java.util.UUID;

/** DTO for {@link com.example.clientservice.entity.Client} */
public record ClientResponseDto(UUID id, 
                                String name, 
                                String surname, 
                                String email) {
}
