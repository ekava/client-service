package com.example.clientservice.dto.keycloak;

public record KeycloakCredentialsDto(
        String type,
        String value,
        Boolean temporary
) {
    public KeycloakCredentialsDto(String value) {
        this("password", value, false);
    }
}
