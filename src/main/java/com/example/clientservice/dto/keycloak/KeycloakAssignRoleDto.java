package com.example.clientservice.dto.keycloak;


public record KeycloakAssignRoleDto(String id,
                                    String name,
                                    String description,
                                    Boolean composite,
                                    Boolean clientRole,
                                    String containerId
                                    ) {
    public KeycloakAssignRoleDto(String id, String name, String containerId) {
        this(id, name, "", false, false, containerId);
    }
}
