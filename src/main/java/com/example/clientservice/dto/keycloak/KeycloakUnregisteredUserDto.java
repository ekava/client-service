package com.example.clientservice.dto.keycloak;

import java.util.UUID;

public record KeycloakUnregisteredUserDto(UUID id,
                                          Boolean emailVerified) {
}
