package com.example.clientservice.dto.keycloak;

public record KeycloakTokenResponseDto(
        String access_token,
        long expires_in,
        long refresh_expires_in,
        String token_type,
        long notBeforePolicy,
        String scope
) {
}