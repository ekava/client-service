package com.example.clientservice.dto.keycloak;


import java.util.Arrays;
import java.util.Objects;

public record KeycloakClientRegisterDto(String username,
                                        String email,
                                        Boolean enabled,
                                        KeycloakCredentialsDto[] credentials
                                        ) {
    public KeycloakClientRegisterDto(String email, String password) {
        this(email, email, true, new KeycloakCredentialsDto[]{new KeycloakCredentialsDto(password)});
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeycloakClientRegisterDto that = (KeycloakClientRegisterDto) o;
        return Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email);
    }

    @Override
    public String toString() {
        return "KeycloakClientRegisterDto{" +
                "email='" + email + '\'' +
                ", credentials=" + Arrays.toString(credentials) +
                '}';
    }
}
