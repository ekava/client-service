package com.example.clientservice.feignclient;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import com.example.clientservice.dto.keycloak.KeycloakAssignRoleDto;
import com.example.clientservice.dto.keycloak.KeycloakClientRegisterDto;
import com.example.clientservice.dto.keycloak.KeycloakUnregisteredUserDto;
import java.util.List;
import java.util.UUID;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "keycloak", url = "${client.keycloak.host}")
public interface KeycloakClient {

    @PostMapping
    void registerUser(@RequestHeader("Authorization") String bearerToken,
                      @RequestBody KeycloakClientRegisterDto registerDto);

    @GetMapping(value = "?email={email}")
    List<KeycloakUnregisteredUserDto> getUsersByEmail(@RequestHeader(AUTHORIZATION) String bearerToken,
                                                      @PathVariable String email);

    @PostMapping(value = "/{userId}/role-mappings/realm")
    void assignRole(@RequestHeader(AUTHORIZATION) String bearerToken,
                    @PathVariable UUID userId,
                    @RequestBody KeycloakAssignRoleDto[] assignRoleDto);
}
