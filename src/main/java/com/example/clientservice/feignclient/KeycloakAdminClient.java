package com.example.clientservice.feignclient;

import com.example.clientservice.dto.keycloak.KeycloakTokenResponseDto;
import java.util.Map;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(value = "keycloak-admin", url = "${env.keycloak.admin-url}")
public interface KeycloakAdminClient {
    @PostMapping(value = "/protocol/openid-connect/token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    KeycloakTokenResponseDto getToken(@RequestHeader("Authorization") String authHeader,
                                      @RequestBody Map<String, ?> formData);
}
