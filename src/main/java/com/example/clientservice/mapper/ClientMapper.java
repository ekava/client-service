package com.example.clientservice.mapper;

import com.example.clientservice.dto.ClientRequestDto;
import com.example.clientservice.dto.ClientResponseDto;
import com.example.clientservice.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Client clientRequestDtoToClient(ClientRequestDto clientRequestDto);

    ClientResponseDto clientToClientResponseDto(Client client);
}
