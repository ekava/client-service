package com.example.clientservice.controller;

import jakarta.annotation.security.RolesAllowed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class HelloController {

    @GetMapping("/hello")
    @RolesAllowed("user")
    public String getHello() {

        return "Hello, World!";
    }

}
