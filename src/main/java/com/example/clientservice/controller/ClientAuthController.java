package com.example.clientservice.controller;

import com.example.clientservice.dto.ClientResponseDto;
import com.example.clientservice.service.interfaces.ClientService;
import jakarta.annotation.security.RolesAllowed;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth/clients")
@RequiredArgsConstructor
public class ClientAuthController {

    private final ClientService clientService;

    @GetMapping
    @RolesAllowed({"user", "worker", "owner"})
    public ClientResponseDto getClientById(@AuthenticationPrincipal Jwt jwt) {
        UUID clientId = UUID.fromString(jwt.getSubject());
        return clientService.getClientById(clientId);
    }
}
