package com.example.clientservice;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled
class ClientServiceApplicationTests {

    @Test
    void contextLoads() {
    }

}
