package com.example.clientservice.service.implementations;

import com.example.clientservice.dto.ClientRequestDto;
import com.example.clientservice.entity.Client;
import com.example.clientservice.mapper.ClientMapper;
import com.example.clientservice.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Disabled
class ClientServiceImplTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private ClientMapper clientMapper;

    @InjectMocks
    private ClientServiceImpl clientService;

    private ClientRequestDto clientRequestDto;
    private Client client;

    @BeforeEach
    void setUp() {
        clientRequestDto = mock(ClientRequestDto.class);
        client = mock(Client.class);
        when(clientMapper.clientRequestDtoToClient(clientRequestDto)).thenReturn(client);
    }

    @Test
    @DisplayName("Test createClient when a client is successfully created then no exception is thrown")
    void testCreateClientWhenClientIsCreatedThenNoExceptionIsThrown() { // TODO write fail test
        // Arrange
        // Act
        assertThatCode(() -> clientService.createClient(clientRequestDto))
                .doesNotThrowAnyException();

        // Assert
        verify(clientMapper).clientRequestDtoToClient(clientRequestDto);
        verify(clientRepository).save(client);
    }
}
